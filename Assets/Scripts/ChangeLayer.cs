﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLayer : MonoBehaviour
{
	public string layerOnEnter;
	public string layerOnExit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("first");
		if (other.tag == "Enemy")
		{
			//Debug.Log("here");
			//Debug.Log(other.name);
			other.gameObject.GetComponent<LayerListener>().Layer = LayerMask.NameToLayer(layerOnEnter);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy")
		{
			//Debug.Log(other.name);
			other.gameObject.GetComponent<LayerListener>().Layer = LayerMask.NameToLayer(layerOnExit);
		}
	}
}
