﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerListener : MonoBehaviour
{
	int currentLayer;

	public delegate void LayerChangeHandler(int cL);
	public event LayerChangeHandler LayerChanged;

	public int Layer
	{
		get
		{
			return gameObject.layer;
		}

		set
		{
			gameObject.layer = value;
			Debug.Log(value);
			if (gameObject.layer != currentLayer)
			{
				currentLayer = gameObject.layer;
				if (LayerChanged != null)
				{
					LayerChanged(currentLayer);
				}
			}
		}
	}

	private void Awake()
	{
		currentLayer = gameObject.layer;
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
