﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPit : MonoBehaviour
{
	public Transform pit;
	Animator anim;
	AudioSource source;
	float delay = 2f;
	float lastUse;
    // Start is called before the first frame update
    void Start()
    {
		anim = GetComponent<Animator>();
		source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.P))
		{
			if (Time.time - lastUse > delay)
			{
				anim.SetTrigger("Point");
				source.Play();
				lastUse = Time.time;
			}
			
		}
    }
}
