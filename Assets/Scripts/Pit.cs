﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pit : MonoBehaviour
{
	public Animator pit;
	public Animator mask;

	//void 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.G))
		{
			pit.SetTrigger("Grow");
			mask.SetTrigger("Grow");
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			pit.SetTrigger("Shrink");
			mask.SetTrigger("Shrink");
		}
    }
}
