﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlRagdoll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		//CharacterJoint[] characterJoints = GetComponentsInChildren<CharacterJoint>();
		//Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
		
		// we avoid calling GetComponentsInChildren on this transform because it will deactivate
		// the main capsule collider
		//Collider[] colliders = transform.GetChild(1).GetComponentsInChildren<Collider>();


		//foreach (var joint in characterJoints)
		//{
		//	//joint.
		//}

		//foreach (var body in rigidbodies)
		//{
		//	body.en
		//}

		//foreach (var collider in colliders)
		//{
		//	collider.enabled = false;
		//}

		GetComponent<LayerListener>().LayerChanged += OnLayerChanged;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnLayerChanged(int c)
	{
		if (c == 10)
		{
			Debug.Log("changed");
			//Collider[] colliders = transform.GetChild(1).GetComponentsInChildren<Collider>();

			////Collider c = GetComponent<Collider>().enabled = fa
			////foreach (var joint in characterJoints)
			////{
			////	//joint.
			////}

			////foreach (var body in rigidbodies)
			////{
			////	body.en
			////}

			//foreach (var collider in colliders)
			//{
			//	collider.enabled = true;
			//	//we also want to set the layer to the current one
			//	collider.gameObject.layer = c;
			//}

			if (transform.parent == null)
			{
				GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
				GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().enabled = false;
				GetComponent<NavMeshAgent>().enabled = false;
				GetComponent<Animator>().enabled = false;
			}
			//GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
			//GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().enabled = false;
			//GetComponent<NavMeshAgent>().enabled = false;
			//GetComponent<Animator>().enabled = false;
		}
		
	}
}
